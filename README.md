# **RESTful web service quering mongodb (python)**

## Exersice Requirements
 Based on the TRADE_ANALYSIS dataset given, build a RESTful web service that can be queried for:
  - the total gain amount for a given customer,
  - the total transaction amount for a given customer, and
  - the number of trades made by a given customer.
 

**TRADE_ANALYSIS DATA STRUCTURE**

|COLUMN                  |  NAME TYPE       |           COMMENT|
|------------------------|--------------------|------------------------------------------|
|ACTIVITY_DATE          |    DATE NOT NULL,     | Date transaction was made  |
|CUSTOMER_ID            |    INTEGER NOT NULL,  | Unique Player identifier  |
|STOCK_ID               |    SMALLINT NOT NULL, | Unique Stock identifier  |
|TRANSACTION_AMOUNT     |    REAL NOT NULL,     | Total amount invested on stock  |
|NUMBER_OF_TRANSACTIONS  |   INTEGER NOT NULL,  | Number of transactions on stock  |
|GAIN_AMOUNT             |   REAL NOT NULL,     | Total amount gained on the stock  |
|ACTIVITY_YEAR_MONTH     |   INTEGER NOT NULL,  | YYYYMM  |
|TRADE_TYPE_ID           |   SMALLINT DEFAULT 0 NOT NULL | 0=Real money, 1=Stock trade  |

The “TRADE_ANALYSIS” table provides a summary of each customer’s trade on a given date, listing the total amounts spent and gained for each stock on that day. If a customer does not spend on a given date, there will be no entries in the table. You can assume this is a very large table ~100M rows.


The solution should also meet the following requirements:
  - All responses should be in JSON format.
  - Each RESTful endpoint should accept an optional parameter for calculating the totals for a specific month, and an optional parameter for a specific stock type.

## Solution approach

Tools used:
  - **PyCharm** as IDE (**python**)
  - **Flask**: is a Python-based microframework that enables you to quickly build web applications
  - Database: **mongodb** in **docker** container
> Because this is a very large table ~100M rows - I use MongoDB

**venv dependencies:**
```
os
flask
flask_restful
flask_pymongo
```



**MongoDB installation steps:**
  
1.  install Docker Desktop Community

2.  create and execute mongodb docker container
```
--create a volume to persist data between runs.
docker volume create --name=mongodata
docker run --name mongodb -v mongodata:/data/db -d -p 27017:27017 mongo
```
3.  access to mongodb bash
```
docker exec -it mongodb bash

mongo
```


4.  create database

`use tradeanalysisdatabase`


5.  create collection

```
db.createCollection( "trade_analysis", {
   validator: { $jsonSchema: {
      bsonType: "object",
      required: [ "activity_date", "customer_id", "stock_id", "transaction_amount", "number_of_transactions", "gain_amount", "activity_year_month", "trade_type_id" ],
      properties: {
         activity_date: {
            bsonType: "date",
            description: "must be a date and is required"
         },
         customer_id: {
            bsonType : "number",
            description: "must be a number and is required"
         },
		 stock_id: {
            bsonType: "number",
            description: "must be a number and is required"
         },
		 transaction_amount: {
            bsonType: "double",
            description: "must be a double and is required"
         },
		 number_of_transactions: {
            bsonType: "number",
            description: "must be a number and is required"
         },
		 gain_amount: {
            bsonType: "double",
            description: "must be a double and is required"
         },
		 activity_year_month: {
            bsonType: "number",
            description: "must be a number and is required"
         },
		 trade_type_id: {
            bsonType: "number",
            description: "must be a number and is required"
         }
      }
   } }
} )

db.trade_analysis.createIndex(
{customer_id: 1, year_month: 1, stock_id: 1},
{name: "query at customer level"}
)
```


6.  insert data

   ```
db.trade_analysis.insert([
   {
      activity_date: ISODate("2017-01-20"), 
      customer_id: 1001,
      stock_id: 987,
      transaction_amount: 0.1,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201701,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-01-27"), 
      customer_id: 1001,
      stock_id: 7499,
      transaction_amount: 0.1,
      number_of_transactions: 1,
      gain_amount: 0.7,
	  activity_year_month: 201701,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-01-27"), 
      customer_id: 1001,
      stock_id: 7499,
      transaction_amount: 0.2,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201701,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-01-27"), 
      customer_id: 1001,
      stock_id: 1320,
      transaction_amount: 0.2,
      number_of_transactions: 1,
      gain_amount: 13.4,
	  activity_year_month: 201701,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-01-05"), 
      customer_id: 1001,
      stock_id: 1293,
      transaction_amount: 0.5,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201701,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-05-04"), 
      customer_id: 1001,
      stock_id: 7610,
      transaction_amount: 0.2,
      number_of_transactions: 1,
      gain_amount: 0.2,
	  activity_year_month: 201705,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-06-13"), 
      customer_id: 1001,
      stock_id: 6773,
      transaction_amount: 6.3,
      number_of_transactions: 3,
      gain_amount: 0,
	  activity_year_month: 201706,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-09-09"), 
      customer_id: 1001,
      stock_id: 2536,
      transaction_amount: 1.2,
      number_of_transactions: 2,
      gain_amount: 0,
	  activity_year_month: 201709,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-09-09"), 
      customer_id: 1001,
      stock_id: 2057,
      transaction_amount: 0.1,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201709,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-12-04"), 
      customer_id: 1001,
      stock_id: 1097,
      transaction_amount: 0.3,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201712,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-12-12"), 
      customer_id: 1001,
      stock_id: 7724,
      transaction_amount: 0.5,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201712,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-12-11"), 
      customer_id: 1001,
      stock_id: 7723,
      transaction_amount: 0.1,
      number_of_transactions: 1,
      gain_amount: 16.5,
	  activity_year_month: 201712,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-03-31"), 
      customer_id: 1002,
      stock_id: 2625,
      transaction_amount: 0.1,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201703,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-03-31"), 
      customer_id: 1002,
      stock_id: 2623,
      transaction_amount: 0.1,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201703,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-03-14"), 
      customer_id: 1002,
      stock_id: 6294,
      transaction_amount: 0.2,
      number_of_transactions: 1,
      gain_amount: 43,
	  activity_year_month: 201703,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-04-20"), 
      customer_id: 1002,
      stock_id: 7588,
      transaction_amount: 6.5,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201704,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-04-26"), 
      customer_id: 1002,
      stock_id: 7499,
      transaction_amount: 14.2,
      number_of_transactions: 4,
      gain_amount: 0,
	  activity_year_month: 201704,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-04-30"), 
      customer_id: 1002,
      stock_id: 2057,
      transaction_amount: 7.3,
      number_of_transactions: 2,
      gain_amount: 0,
	  activity_year_month: 201704,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-04-18"), 
      customer_id: 1002,
      stock_id: 7596,
      transaction_amount: 7.14,
      number_of_transactions: 2,
      gain_amount: 7.14,
	  activity_year_month: 201704,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-08-26"), 
      customer_id: 1002,
      stock_id: 6435,
      transaction_amount: 3,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201708,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-09-03"), 
      customer_id: 1002,
      stock_id: 5632,
      transaction_amount: 3,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201709,
	  trade_type_id: 0
   },
   {
      activity_date: ISODate("2017-06-14"), 
      customer_id: 1003,
      stock_id: 4326,
      transaction_amount: 1,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201706,
	  trade_type_id: 1
   },
   {
      activity_date: ISODate("2017-06-15"), 
      customer_id: 1003,
      stock_id: 5876,
      transaction_amount: 1,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201706,
	  trade_type_id: 1
   },
   {
      activity_date: ISODate("2017-07-03"), 
      customer_id: 1003,
      stock_id: 8565,
      transaction_amount: 1,
      number_of_transactions: 1,
      gain_amount: 0,
	  activity_year_month: 201707,
	  trade_type_id: 1
   }
])
```

**Web Service execution:**
  
1.  PyCharm --> run 'run.py'

2.  Web Navigator (Google Chrome) --> Access to http://127.0.0.1:5000/api/Query....


## Testing Endpoints

  - **Query1:** the total gain amount for a given customer,
  - **Query2:** the total transaction amount for a given customer, and
  - **Query3:** the number of trades made by a given customer.
  >Each RESTful endpoint should accept an optional parameter for calculating the totals for a specific month, and an optional parameter for a specific stock type.

http://127.0.0.1:5000/api/Query1/1001
```
{
  "result": [
    {
      "customer_id": 1001, 
      "total_gain_amount": 30.8
    }
  ]
}
```



http://127.0.0.1:5000/api/Query2/1001/201709
```
{
  "result": [
    {
      "activity_year_month": 201709, 
      "customer_id": 1001, 
      "total_transaction_amount": 1.3
    }
  ]
}
```



http://127.0.0.1:5000/api/Query3/1001/201701/987
```
{
  "result": [
    {
      "activity_year_month": 201701, 
      "customer_id": 1001, 
      "stock_id": 987, 
      "total_number_of_transactions": 1.0
    }
  ]
}
```




