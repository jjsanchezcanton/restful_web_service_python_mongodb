from flask_restful import Resource
from flask import Flask
from flask import jsonify
from flask_pymongo import PyMongo


# the number of trades made by a given customer
class Query3MonthStock(Resource):

    def get(self, customer_id, year_month, stock_id):
        app = Flask(__name__)

        app.config.from_object("config")

        mongo = PyMongo(app)
        trade_analysis = mongo.db.trade_analysis
        output = []
        pipeline = [
            {"$match": {"customer_id": customer_id, "activity_year_month": year_month, "stock_id": stock_id}},
            {"$group": {"_id": {"customer_id": "$customer_id", "activity_year_month": "$activity_year_month", "stock_id": "stock_id"},
                        "total": {"$sum": "$number_of_transactions"}}}
        ]
        for s in trade_analysis.aggregate(pipeline):
            output.append({'customer_id': customer_id, 'activity_year_month': year_month, 'stock_id': stock_id, 'total_number_of_transactions': s['total']})
        return jsonify({'result': output})
