from flask_restful import Resource
from flask import Flask
from flask import jsonify
from flask_pymongo import PyMongo


# the total gain amount for a given customer
class Query1(Resource):

    def get(self, customer_id):
        app = Flask(__name__)

        app.config.from_object("config")

        mongo = PyMongo(app)
        trade_analysis = mongo.db.trade_analysis
        output = []
        pipeline = [
            { "$match": { "customer_id": customer_id } },
            { "$group": { "_id": "$customer_id", "total": { "$sum": "$gain_amount" } } }
]
        for s in trade_analysis.aggregate(pipeline):
            output.append({'customer_id': customer_id,'total_gain_amount': s['total']})
        return jsonify({'result': output})
