from flask_restful import Resource
from flask import Flask
from flask import jsonify
from flask_pymongo import PyMongo


# the number of trades made by a given customer
class Query3(Resource):

    def get(self, customer_id):
        app = Flask(__name__)

        app.config.from_object("config")

        mongo = PyMongo(app)
        trade_analysis = mongo.db.trade_analysis
        output = []
        pipeline = [
            { "$match": { "customer_id": customer_id } },
            { "$group": { "_id": "$customer_id", "total": { "$sum": "$number_of_transactions" } } }
]
        for s in trade_analysis.aggregate(pipeline):
            output.append({'customer_id': customer_id,'total_number_of_transactions': s['total']})
        return jsonify({'result': output})
