from flask_restful import Resource
from flask import Flask
from flask import jsonify
from flask_pymongo import PyMongo


# the total gain amount for a given customer
class Query1MonthStock(Resource):

    def get(self, customer_id, year_month, stock_id):
        app = Flask(__name__)

        app.config.from_object("config")

        mongo = PyMongo(app)
        trade_analysis = mongo.db.trade_analysis
        output = []
        pipeline = [
            {"$match": {"customer_id": customer_id, "activity_year_month": year_month, "stock_id": stock_id}},
            {"$group": {"_id": {"customer_id": "$customer_id", "activity_year_month": "$activity_year_month", "stock_id": "stock_id"},
                        "total": {"$sum": "$gain_amount"}}}
        ]
        for s in trade_analysis.aggregate(pipeline):
            output.append({'customer_id': customer_id, 'activity_year_month': year_month, 'stock_id': stock_id, 'total_gain_amount': s['total']})
        return jsonify({'result': output})
