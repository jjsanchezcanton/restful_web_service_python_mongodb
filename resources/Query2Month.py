from flask_restful import Resource
from flask import Flask
from flask import jsonify
from flask_pymongo import PyMongo


# the total transaction amount for a given customer
class Query2Month(Resource):

    def get(self, customer_id, year_month):
        app = Flask(__name__)

        app.config.from_object("config")

        mongo = PyMongo(app)
        trade_analysis = mongo.db.trade_analysis
        output = []
        pipeline = [
            {"$match": {"customer_id": customer_id, "activity_year_month": year_month}},
            {"$group": {"_id": {"customer_id": "$customer_id", "activity_year_month": "$activity_year_month"},
                        "total": {"$sum": "$transaction_amount"}}}
        ]
        for s in trade_analysis.aggregate(pipeline):
            output.append({'customer_id': customer_id, 'activity_year_month': year_month, 'total_transaction_amount': s['total']})
        return jsonify({'result': output})
