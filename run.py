from flask import Flask
from flask import Blueprint
from flask_restful import Api
from resources.Query1 import Query1
from resources.Query1Month import Query1Month
from resources.Query1MonthStock import Query1MonthStock
from resources.Query2 import Query2
from resources.Query2Month import Query2Month
from resources.Query2MonthStock import Query2MonthStock
from resources.Query3 import Query3
from resources.Query3Month import Query3Month
from resources.Query3MonthStock import Query3MonthStock



def create_app():
    app = Flask(__name__)

    api_bp = Blueprint('api', __name__)
    api = Api(api_bp)
    app.register_blueprint(api_bp, url_prefix='/api')

    # # Routes
    api.add_resource(Query1, '/Query1/<int:customer_id>')
    api.add_resource(Query1Month, '/Query1/<int:customer_id>/<int:year_month>')
    api.add_resource(Query1MonthStock, '/Query1/<int:customer_id>/<int:year_month>/<int:stock_id>')
    api.add_resource(Query2, '/Query2/<int:customer_id>')
    api.add_resource(Query2Month, '/Query2/<int:customer_id>/<int:year_month>')
    api.add_resource(Query2MonthStock, '/Query2/<int:customer_id>/<int:year_month>/<int:stock_id>')
    api.add_resource(Query3, '/Query3/<int:customer_id>')
    api.add_resource(Query3Month, '/Query3/<int:customer_id>/<int:year_month>')
    api.add_resource(Query3MonthStock, '/Query3/<int:customer_id>/<int:year_month>/<int:stock_id>')

    return app


if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)