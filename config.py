import os

# You need to replace the next values with the appropriate values for your configuration

basedir = os.path.abspath(os.path.dirname(__file__))
MONGO_DBNAME = "tradeanalysisdatabase"
# MONGO_URI = "mongodb://username:password@host[:port]/database_name"
MONGO_URI = "mongodb://localhost:27017/tradeanalysisdatabase"
